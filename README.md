# Demonstrate the difficulties of maintaining forward and backward compatibility in C++

This project provides a harness to showcase certain difficulties with
ABI compatibility in C++. It was developed in the context of a project
that attempted to move from [protobuf](https://github.com/protocolbuffers/protobuf)
which offers both [backward and forward compatibility](https://protobuf.dev/overview/#updating-defs)
at the _data_ level. The project now attempted to get rid of protobuf
and provide a purely C++-based API instead. Client code gets recompiled
seldom (if ever) - thus the strict compatibility requirements.

The examples in this project hopefully make obvious that that endeavour
is at the level of C++ standard library implementers and not to be taken
lightly.

## Mode of operation

* there are two submodules to represent the vendor and client code
  - both repositories have tags representing the API versions provided
    by the vendor and the uses of that versions by the client
* there is a script [run-demo.sh](run-demo.sh) that switches the
  submodules to any of these tags, builds and installs them and then
  runs them to show the problems.

## Prerequisites

* a UNIX operating system
* at least CMake v3.17.5 (the one currently available on EPEL7)
* a C++ compiler (preferably the latest release)

## Setup

```sh
git clone --recurse-submodules https://gitlab.com/oburns/cpp-forward-backward-compatibility
./run-demo.sh
```

## Related

* [libstdc++: ABI Policy and Guidelines](https://gcc.gnu.org/onlinedocs/libstdc++/manual/abi.html)
* [ABI compliance checker](https://lvc.github.io/abi-compliance-checker)
* [What is an ABI, and Why is Breaking it Bad? - Marshall Clow - CppCon 2020](https://www.youtube.com/watch?v=7RoTDjLLXJQ)
