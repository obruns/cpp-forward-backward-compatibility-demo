#!/bin/sh
set -e -o pipefail
set -u
set -x

#
# Case 1: vendor and client are at the same API
#

git -C vendor checkout --quiet v1.0.0
rm --recursive --force vendor/build &&
    CXX=g++ cmake -S vendor -B vendor/build -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON &&
    cmake --build vendor/build -- --jobs V=1
cmake --install vendor/build --prefix /tmp/testvendor

git -C client checkout --quiet v1.0.0
rm --recursive --force client/build &&
    CXX=g++ cmake -S client -B client/build -DCMAKE_PREFIX_PATH=/tmp/testvendor/lib/cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON &&
    cmake --build client/build -- --jobs V=1
cmake --install client/build --prefix /tmp/testclient

vendor/build/runner /tmp/testclient/lib/libclient.so | grep --quiet 1

#
# Case 2: vendor API has advanced to v2
#

git -C vendor checkout --quiet v2.0.0
rm --recursive --force vendor/build &&
    CXX=g++ cmake -S vendor -B vendor/build -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON &&
    cmake --build vendor/build -- --jobs V=1
cmake --install vendor/build --prefix /tmp/testvendor

# We do not do anything about the client and reuse the artifact from case 1

vendor/build/runner /tmp/testclient/lib/libclient.so | grep --quiet 2251795519242239
# The previous value is 0x7ffff0007ffff which is what the two 32-bit
# integers have been initialized to but interpreted as one 64-bit integer.
